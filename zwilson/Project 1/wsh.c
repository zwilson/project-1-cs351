// Zach Wilson
//
// this file is simply the main function for the wsh. it gets the commands from
// std input and passes them to the jobs function. it also prints out wsh:

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "jobs.h"

int main(){
  char buffer[257];
  int size = 257;

  while(1){
    printf("wsh: ");
    fgets(buffer, size, stdin);
    job(buffer);
  }
  return 0;
}
