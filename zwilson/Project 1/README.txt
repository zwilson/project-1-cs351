Wimpy Shell by Zach Wilson

Files:

jobs.c and jobs.h: contain all logic for executing commands
wsh.c: contains the main loop of the program

Build command: make
Run command: ./wsh

IMPORTANT:

Arguments for commands as well as pipes and file redirect symbols must be seperated
with spaces

Bad:
ls-a|cat

Good:
ls -a | cat
