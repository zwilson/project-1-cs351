// Zach Wilson
//
// This file contains all logic for parsing, executing and managing commands. all
// commands are given to the jobs() function and it parses and executs the command.
// jobs() also handles built in commands cd, exit, and wait. this file also
// contains teh globals that store background jobs

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>
#include <fcntl.h>
//max amount of background jobs that can be tracked
#define MAX_BACKGROUND_JOBS 50

//struct that contains information for a background job
typedef struct background_job {
  int job_num; //job num associated with the job
  int pid; //process id associated with the job
  char command[257]; //stores the command that started the job
  int io_fd[2]; //represents the input and output file descriptors so they can be closed
} background_job_t;

int job_num = 1; //current job number
int background_jobs = 0; //total amount of background jobs that are running
int finished_jobs = 0; //total amount of background jobs that have finished
background_job_t running[MAX_BACKGROUND_JOBS]; //stores running jobs
background_job_t finished[MAX_BACKGROUND_JOBS]; //stores finished jobs


//splits the command up by spaces, returns size of arg array not including
//NULL terminator
int make_arguments(char *command, char **arguments){
  int i = 0;
  char *tok = strtok(command, " ");

  while(tok != NULL){
    arguments[i] = tok;
    tok = strtok(NULL, " ");
    i++;
  }

  //remove the newline character from the last argument if it exists
  if(arguments[i - 1][strlen(arguments[i - 1]) - 1] == '\n'){
    arguments[i - 1][strlen(arguments[i - 1]) - 1] = '\0';
  }
  //adding spaces to the end of a line causes an empty string at the end of the array
  if(strlen(arguments[i - 1]) == 0){
    arguments[i - 1] = NULL;
    return i - 1;
  }
  else{
    arguments[i] = NULL;
    return i;
  }
}

//prints command not found warning given the arguments array
void print_command_not_found(char **arg){
  int i;
  printf("Command not found: ");
  for(i = 0; arg[i]; i++){
    printf("%s ", *arg);
  }
  printf("\n");
}

//executes a job in the foreground
//arg: argments array for command
//infd: input file descriptor for command
//outfd: output file escriptr for command
int foreground_job(char **arg, int infd, int outfd){
  pid_t id = 0;
  int status = 0;

  id = fork();

  if(id){
    //parent wait for child
    waitpid(id, &status, 0);
    if(WEXITSTATUS(status) == 255){
      print_command_not_found(arg);
    }
    if(infd > 1){
      close(infd);
    }
    if(outfd > 1){
      close(outfd);
    }
  }
  else if(id == 0){
    //set in and out files
    if(infd > 1){
      dup2(infd, 0);
    }
    if(outfd > 1){
      dup2(outfd, 1);
    }
    if(execvp(arg[0], arg) < 0){
      exit(-1);
    }
  }
}

//prints the status of all jobs that have finnished and that are running
void print_background_jobs(){
  if(background_jobs == 0 && finished_jobs == 0){
    return;
  }
  int i;
  printf("Running: %d\n", background_jobs);
  for(i = 0; i < background_jobs; i++){
    printf("\t[%d] %s", running[i].job_num, running[i].command);
  }
  printf("Finished: %d\n", finished_jobs);
  for(i = 0; i < finished_jobs; i++){
    printf("\t[%d] %s", finished[i].job_num, finished[i].command);
  }
  //reset finished jobs
  finished_jobs = 0;
  if(background_jobs == 0){
    job_num = 1;
  }
}

//checks on background jobs and moves them to finished if they have completed
void check_background_jobs(){
  int i, j, status = 0;

  for(i = 0; i < background_jobs; i++){
    //if the background job has not exited continue
    if(waitpid(running[i].pid, &status, WNOHANG) == 0){
      continue;
    }
    //if it has exited close the files it was using
    if(running[i].io_fd[0] > 1){
      close(running[i].io_fd[0]);
    }
    if(running[i].io_fd[1] > 1){
      close(running[i].io_fd[1]);
    }
    //clear it from finished and move it to running
    finished[finished_jobs] = running[i];
    finished_jobs++;
    for(j = i; j < background_jobs - 1; j++){
      running[j] = running[j + 1];
    }
    background_jobs--;
  }
}

//executes a job in the background
//arg: argments array for command
//infd: input file descriptor for command
//outfd: output file escriptr for command
void background_job(char **arg, char *command, int infd, int outfd){

  if(background_jobs >= MAX_BACKGROUND_JOBS){
    printf("There are currently too many background jobs\n");
    return;
  }

  int pid = 0, status = 0;
  background_job_t job;

  pid = fork();
  if(pid == 0){
    //set in and out files
    if(infd > 1){
      dup2(infd, 0);
    }
    if(outfd > 1){
      dup2(outfd, 1);
    }
    if(execvp(arg[0], arg) < 0){
      exit(-1);
    }
  }
  //wait a short amount of time so that we can see if the command
  //was a valid command without blocking
  nanosleep((const struct timespec[]){{0, 1000L}}, NULL);
  waitpid(pid, &status, WNOHANG);
  if(WEXITSTATUS(status) == 255){
    print_command_not_found(arg);
    //close open files
    if(infd > 1){
      close(infd);
    }
    if(outfd > 1){
      close(outfd);
    }
    return;
  }
  //put command into running array
  job.job_num = job_num;
  job.pid = pid;
  strcpy(job.command, command);
  job.io_fd[0] = infd;
  job.io_fd[0] = infd;

  running[background_jobs] = job;
  background_jobs++;
  job_num++;
}

//executes a pipeline command
//arg: arguments array where each pipelined command is seperated by a NULL pointer
//jobs: number of jobs or commands in the pipeline
//index: array of indexs of the commands in the arg array
void pipeline_job(char **arg, int jobs, int *index){
  int i, j, status = 0;
  int pids[130];
  int pipefd_a[2];
  int pipefd_b[2];

  pipe(pipefd_a);

  //create first process in chain
  pids[0] = fork();
  if(pids[0] == 0){
    //write to pipe a
    dup2(pipefd_a[1], 1);
    close(pipefd_a[0]);
    //if execvp fails exit with its exit status of -1
    if(execvp(arg[0], &arg[0]) < 0){
      exit(-1);
    }
  }
  //middle jobs
  for(i = 1; i < jobs - 1; i++){
    if(i % 2 == 0){
      //create new pipe a to be written to
      pipe(pipefd_a);
      pids[i] = fork();
      if(pids[i] == 0){
        //read from pipe b
        dup2(pipefd_b[0], 0);
        //write to pipe a
        dup2(pipefd_a[1], 1);
        close(pipefd_a[0]);
        close(pipefd_b[1]);
        //if execvp fails exit with its exit status of -1
        if(execvp(arg[index[i]], &arg[index[i]]) < 0){
          exit(-1);
        }
      }
      //close pipe b in the parent since it is not needed anymore
      close(pipefd_b[0]);
      close(pipefd_b[1]);
    }else{
      //create new pipe b to be written to
      pipe(pipefd_b);
      pids[i] = fork();
      if(pids[i] == 0){
        //read from a pipe
        dup2(pipefd_a[0], 0);
        //write to b pipe
        dup2(pipefd_b[1], 1);
        close(pipefd_a[1]);
        close(pipefd_b[0]);
        //if execvp fails exit with its exit status of -1
        if(execvp(arg[index[i]], &arg[index[i]]) < 0){
          exit(-1);
        }
      }
      //close pipe a in the parent since it is not needed anymore
      close(pipefd_a[0]);
      close(pipefd_a[1]);
    }
  }

  pids[i] = fork();
  if(pids[i] == 0){
    if(i % 2 == 0){
      //read from pipe b
      dup2(pipefd_b[0], 0);
      close(pipefd_b[1]);
    }else{
      //read from pipe a
      dup2(pipefd_a[0], 0);
      close(pipefd_a[1]);
    }
    if(execvp(arg[index[i]], &arg[index[i]]) < 0){
      exit(-1);
    }
  }

  if(i % 2 == 0){
    //close pipe b
    close(pipefd_b[0]);
    close(pipefd_b[1]);
  }else{
    //close pipe a
    close(pipefd_a[0]);
    close(pipefd_a[1]);
  }

  for(i = 0; i < jobs; i++){
    waitpid(pids[i], &status, 0);
    if(WEXITSTATUS(status) == 255){
      print_command_not_found(&arg[index[i]]);
    }
  }
}

//finds input and output files for a given command
//arg: argument array
//infd: where to store the input file descriptor if found, 0 if not found
//outfd: where to store the output file descriptor if found, 0 if not found
void find_io_files(char **arg, int *infd, int *outfd){
  int in_index = 5000, out_index = 5000;
  int i = 0;

  //look for input file
  for(i = 0; arg[i] != NULL; i++){
    if(strcmp(arg[i], "<") == 0){
      if(arg[i + 1]){
        *infd = open(arg[i + 1], O_RDONLY);
        if(*infd < 0){
          printf("%s is an invalid file\n", arg[i + 1]);
          return;
        }
        //if we are here we have found a valid input file
        in_index = i;
        break;
      }else{
        printf("No input file specified\n");
        return;
      }
    }
  }
  //look for output file
  for(i = 0; arg[i] != NULL; i++){
    if(strcmp(arg[i], ">") == 0){
      if(arg[i + 1]){
        *outfd = open(arg[i + 1], O_CREAT | O_WRONLY | O_TRUNC, S_IRWXU | S_IROTH);
        if(*outfd < 0){
          printf("Could not open the output file\n");
          return;
        }
        //successfully created or opened output file
        out_index = i;
      }else{
        printf("No output file specified\n");
        return;
      }
    }
  }
  //get rid of the input and output arguments
  if(in_index != 5000 && in_index <= out_index){
    arg[in_index] = NULL;
  }
  else if(out_index != 5000 && out_index <= in_index){
    arg[out_index] = NULL;
  }

}


//finds pipes and replaces them with NULL pointers, returns number of commands to
//also fills index array w/ indicies of the individual commands, -1 represents end of array
//pipeline, 0 if no pipes, or -1 if pipes are illegally placed
int find_pipes(char **arg, int *index){
  int i = 0, count = 0, j = 0, last_pipe = -10;

  index[j] = 0;
  j++;

  while(arg[i]){
    if(strcmp(arg[i], "|") == 0){
      //two pipes in a row or pipe at the beginning or pipe at end
      if(last_pipe == i - 1 || i == 0 || arg[i + 1] == NULL){
        return -1;
      }
      index[j] = i + 1;
      j++;
      arg[i] = NULL;
      count++;
      last_pipe = i;
    }
    i++;
  }

  //1 more command than there are pipes
  return count + 1;

}

//changed directory to the given path
void change_directory(char *path){
  char buffer[257];
  if(!path){
    return;
  }
  if(chdir(path) < 0){
    printf("Invalid path: %s\n", path);
  }else{
    getcwd(buffer, 257);
    printf("%s\n", buffer);
  }
}

//waits for the given job and prints jobs statuses when it completes
//job: string with job number
void wait_for_job(char *job){
  if(job == NULL){
    printf("No job number specified\n");
    return;
  }
  int job_num = 0, i = 0, index = -1, status = 0;

  job_num = atoi(job);
  if(job_num == 0){
    printf("Illegal job number\n");
    return;
  }
  //find the job to wait for
  for(i = 0; i < background_jobs; i++){
    if(running[i].job_num == job_num){
      index = i;
    }
  }
  if(index > -1){
    waitpid(running[index].pid, &status, 0);
    //clean up job and move it to finished
    if(running[index].io_fd[0] > 1){
      close(running[index].io_fd[0]);
    }
    if(running[index].io_fd[1] > 1){
      close(running[index].io_fd[1]);
    }
    finished[finished_jobs] = running[index];
    finished_jobs++;
    for(i = index; i < background_jobs - 1; i++){
      running[i] = running[i + 1];
    }
    background_jobs--;
    //check on status of other jobs and print
    check_background_jobs();
    print_background_jobs();
  }else{
    printf("Job not found\n");
  }
}

//waits for all background jobs to complete and then exits
void wait_and_exit(){
  int i, j, status;

  for(i = 0; i < background_jobs; i++){
    waitpid(running[i].pid, &status, 0);
    if(running[i].io_fd[0] > 1){
      close(running[i].io_fd[0]);
    }
    if(running[i].io_fd[1] > 1){
      close(running[i].io_fd[1]);
    }
    finished[finished_jobs] = running[j];
    finished_jobs++;
    for(j = i; i < background_jobs - 1; j++){
      running[j] = running[j + 1];
    }
    background_jobs--;
  }
  print_background_jobs();
  exit(0);
}

void job(char *command){
  //130 * 2 = 260 which is enough space to handle max amount of args in 256 characters
  char *arg[130];
  //stores indexs of commands for pipelining
  int index[130];
  //store the command string since the make_arguments function mangles it
  char command_dup[257];
  int size = 0, i = 0, jobs = 0, outfd = 0, infd = 0, background = 0;

  strcpy(command_dup, command);

  size = make_arguments(command, arg);

  if(size == 0){
    return;
  }

  //handle change directory command
  if(strcmp(arg[0], "cd") == 0){
    change_directory(arg[1]);
    return;
  }
  if(strcmp(arg[0], "wait") == 0){
    wait_for_job(arg[1]);
    return;
  }
  if(strcmp(arg[0], "exit") == 0){
    wait_and_exit();
    return;
  }

  if(background = strcmp(arg[size - 1], "&") == 0){
    arg[size - 1] = NULL;
  }

  jobs = find_pipes(arg, index);

  if(jobs < 0){
    return;
  }else if(jobs > 1){
    pipeline_job(arg, jobs, index);
  }else{
    //get possible input and output files
    find_io_files(arg, &infd, &outfd);

    if(background){
      background_job(arg, command_dup, infd, outfd);
    }else{
      foreground_job(arg, infd, outfd);
    }
  }
  check_background_jobs();
  print_background_jobs();
}
